import { Component } from '@angular/core';
import { NavController,Platform } from 'ionic-angular';
import { InAppBrowser, InAppBrowserOptions } from "@ionic-native/in-app-browser";
import { StatusBar } from '@ionic-native/status-bar';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  url: string='http://kawebook.com'; 
  theme :string[] = [
".bg-main,.feed-board,.nav-menu {background-color: #000 !important;} .readstory-content p,.readstory-storyname,.readstory-content span{color: #FFF !important;}",
".bg-main,.feed-board,.nav-menu {background-color: #f4f4f4 !important;} .readstory-content p,.readstory-storyname,.readstory-content span{color: #000 !important;}" ,
  ];
   browser :any;
  
  options : InAppBrowserOptions = {
    location : 'no',//Or 'no' 
    hidden : 'no', //Or  'yes'
    // clearcache : 'no',
    //  clearsessioncache :'no',
    zoom : 'no',//Android only ,shows browser zoom controls 
    hardwareback : 'yes',
    mediaPlaybackRequiresUserAction : 'yes',
    shouldPauseOnSuspend : 'no', //Android only 
    closebuttoncaption : 'Close', //iOS only
    disallowoverscroll : 'no', //iOS only 
    toolbar : 'yes', //iOS only 
    enableViewportScale : 'yes', //iOS only 
    allowInlineMediaPlayback : 'yes',//iOS only 
    presentationstyle : 'fullscreen',//iOS only 
    fullscreen : 'yes',//Windows only  
    footercolor:'#666666' , 
    toolbarposition:'top',
    toolbarcolor:'#666666',
    navigationbuttoncolor:'#d9d9d9',
    suppressesIncrementalRendering:'yes',
};
  constructor(public navCtrl: NavController,private inAppBrowser: InAppBrowser,public plt: Platform,public statusBar: StatusBar) {
    
  }
  openWebpage(url: string,selecttheme:number) {

     
    if (this.plt.is('ios')) {
      this.browser = this.inAppBrowser.create(url, '_blank', this.options);
    }else{
      this.browser  = this.inAppBrowser.create(url, '_blank');
    }
   // Inject scripts, css and more with browser.X
   this.browser.on('loadstart').subscribe(event => {
   
    this.statusBar.styleBlackTranslucent();
     
 });

   this.browser.on('loadstop').subscribe(event => {
     
    this.browser.insertCSS({ code: this.theme[selecttheme]});
    
 });

  }
}
